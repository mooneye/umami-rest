The UmamiNation REST Platform
=============================
Install
-------
With the amazing [**Composer**][1]

What's inside?
--------------
UmamiNation REST Platform provides:
* The mighty [**API Platform Core Library**][2]
* The incredible [**Symfony Framework**][3]
* UmamiNationRestBundle you can use to start coding

Credits
-------
Created by [Geraldes Pereira | Mooneye][4].

[1]: https://getcomposer.org
[2]: https://api-platform.com
[3]: https://symfony.com
[4]: http://www.mooneye.de
