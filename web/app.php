<?php

use Symfony\Component\HttpFoundation\Request;
use UmamiNation\RestPlatform\AppKernel;

require dirname(__DIR__).'/vendor/autoload.php';
$kernel = new AppKernel('prod', false);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
