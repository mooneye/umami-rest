<?php

namespace UmamiNation\RestPlatform\UmamiNationRestBundle;

use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use UmamiNation\RestPlatform\UmamiNationRestBundle\DependencyInjection\Security\TokenListenerFactory;

/**
 * Class UmamiNationRestBundle.
 */
class UmamiNationRestBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     *
     * @throws LogicException
     */
    public function build(ContainerBuilder $container)
    {
        /** @var SecurityExtension $extension */
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new TokenListenerFactory());
    }
}
