<?php

namespace UmamiNation\RestPlatform\UmamiNationRestBundle\Security;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

/**
 * Class TokenListener.
 */
class TokenListener implements ListenerInterface
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    /**
     * @var string
     */
    private $providerKey;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var null|AuthenticationFailureHandlerInterface
     */
    private $failureHandler;

    /**
     * @var AuthenticationSuccessHandlerInterface
     */
    private $successHandler;

    /**
     * @param TokenStorageInterface          $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param $providerKey
     * @param AuthenticationSuccessHandlerInterface      $successHandler
     * @param AuthenticationFailureHandlerInterface|null $failureHandler
     * @param array                                      $options
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $authenticationManager,
        $providerKey,
        AuthenticationSuccessHandlerInterface $successHandler,
        AuthenticationFailureHandlerInterface $failureHandler = null,
        array $options = null
    ) {
        if (empty($providerKey)) {
            throw new \InvalidArgumentException('$providerKey must not be empty.');
        }

        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->providerKey = $providerKey;
        $this->successHandler = $successHandler;
        $this->failureHandler = $failureHandler;
        $this->options = \array_merge([
            'username_param' => 'username',
            'password_param' => 'password',
        ], $options);
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     * @throws BadRequestHttpException
     * @throws \InvalidArgumentException
     * @throws \HttpResponseException
     * @throws AuthenticationException
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $parameterBag = $this->getParameterBag($request);

        $username = \trim($parameterBag->get($this->options['username_param']));
        $password = $parameterBag->get($this->options['password_param']);
        $token = new UsernamePasswordToken($username, $password, $this->providerKey);

        try {
            $authenticatedToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authenticatedToken);
            $response = $this->onSuccess($request, $authenticatedToken);
        } catch (AuthenticationException $exception) {
            if (null === $this->failureHandler) {
                throw $exception;
            }

            $response = $this->onFailure($request, $exception);
        }

        $event->setResponse($response);
    }

    /**
     * @param Request $request
     *
     * @return ParameterBag
     *
     * @throws \LogicException
     */
    private function getParameterBag(Request $request): ParameterBag
    {
        return $request->isMethod('POST') ? $request->request : $request->query;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return Response
     *
     * @throws \HttpResponseException
     */
    private function onSuccess(Request $request, TokenInterface $token): Response
    {
        $response = $this->successHandler->onAuthenticationSuccess($request, $token);
        if (!$response instanceof Response) {
            throw new \HttpResponseException('Authentication Success Handler did not return a Response.');
        }

        return $response;
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response
     *
     * @throws \HttpResponseException
     */
    private function onFailure(Request $request, $exception): Response
    {
        $response = $this->failureHandler->onAuthenticationFailure($request, $exception);
        if (!$response instanceof Response) {
            throw new \HttpResponseException('Authentication Failure Handler did not return a Response.');
        }

        return $response;
    }
}
