<?php

namespace UmamiNation\RestPlatform\UmamiNationRestBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\OutOfBoundsException;
use Symfony\Component\DependencyInjection\Reference;
use UmamiNation\RestPlatform\UmamiNationRestBundle\Security\TokenListener;

/**
 * Class TokenListenerFactory.
 */
class TokenListenerFactory implements SecurityFactoryInterface
{
    const OPTIONS_AUTH_PROVIDER = 'authentication_provider';
    const OPTIONS_FAILURE_HANDLER = 'failure_handler';
    const OPTIONS_SUCCESS_HANDLER = 'success_handler';
    const OPTIONS_USER_CHECKER = 'user_checker';
    const OPTIONS_USERNAME_PARAM = 'username_param';
    const OPTIONS_PASSWD_PARAM = 'password_param';

    const AUTH_PROVIDER_ID_PREFIX = 'security.authentication.provider.umami-nation.rest.';
    const LISTENER_ID_PREFIX = 'security.authentication.listener.umami-nation.rest.';
    const TOKEN_LISTENER_CONFIG_KEY = 'token_listener_config';

    const PROVIDER_CALL_POSITION = 'pre_auth';

    /**
     * Configures the services required.
     *
     * @param ContainerBuilder $container
     * @param string           $id
     * @param array            $config
     * @param string           $userProvider
     * @param string           $defaultEntryPoint
     *
     * @return array
     *
     * @throws OutOfBoundsException
     * @throws BadMethodCallException
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint): array
    {
        $provider = self::AUTH_PROVIDER_ID_PREFIX.$id;

        $container
            ->setDefinition($provider, new ChildDefinition($config[self::OPTIONS_AUTH_PROVIDER]))
            ->replaceArgument(0, new Reference($userProvider))
            ->replaceArgument(1, new Reference($config[self::OPTIONS_USER_CHECKER]))
            ->replaceArgument(2, $id);

        $listenerId = self::LISTENER_ID_PREFIX.$id;
        $listener = $container
            ->setDefinition(
                $listenerId,
                new ChildDefinition(TokenListener::class)
            )
            ->replaceArgument(2, $id)
            ->replaceArgument(5, $config);

        if (isset($config[self::OPTIONS_SUCCESS_HANDLER])) {
            $listener->replaceArgument(3, new Reference($config[self::OPTIONS_SUCCESS_HANDLER]));
        }

        if (isset($config[self::OPTIONS_FAILURE_HANDLER])) {
            $listener->replaceArgument(4, new Reference($config[self::OPTIONS_FAILURE_HANDLER]));
        }

        return [$provider, $listenerId, $defaultEntryPoint];
    }

    /**
     * Position the provider is called.
     * Possible values: pre_auth, form, http, remember_me.
     *
     * @return string
     */
    public function getPosition(): string
    {
        return self::PROVIDER_CALL_POSITION;
    }

    /**
     * Key used to reference the listener config in the firewall.
     *
     * @return string
     */
    public function getKey(): string
    {
        return self::TOKEN_LISTENER_CONFIG_KEY;
    }

    /**
     * @param NodeDefinition $builder
     */
    public function addConfiguration(NodeDefinition $builder)
    {
        $builder
            ->children()
                ->scalarNode(self::OPTIONS_USERNAME_PARAM)
                    ->defaultValue('username')
                ->end()
                ->scalarNode(self::OPTIONS_PASSWD_PARAM)
                    ->defaultValue('password')
                ->end()
                ->scalarNode(self::OPTIONS_SUCCESS_HANDLER)
                    ->defaultValue('lexik_jwt_authentication.handler.authentication_success')
                ->end()
                ->scalarNode(self::OPTIONS_FAILURE_HANDLER)
                    ->defaultValue('lexik_jwt_authentication.handler.authentication_failure')
                ->end()
                ->scalarNode(self::OPTIONS_AUTH_PROVIDER)
                    ->defaultValue('security.authentication.provider.dao')
                ->end()
                ->scalarNode(self::OPTIONS_USER_CHECKER)
                    ->defaultValue('security.user_checker')
                    ->treatNullLike('security.user_checker')
                ->end()
            ->end();
    }
}
