<?php

namespace UmamiNation\RestPlatform\UmamiNationRestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Table(name="orders")
 * @ORM\Entity
 * @ApiResource
 */
class Order
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="UmamiNation\RestPlatform\UmamiNationRestBundle\Entity\Product")
     * @ORM\JoinColumn(name="product", referencedColumnName="id", nullable=false)
     */
    private $product;

    /**
     * @var \DateTime
     * @ORM\Column(name="order_start", type="datetime", nullable=false)
     */
    private $orderStart;

    /**
     * @var \DateTime
     * @ORM\Column(name="kitchen_start", type="datetime", nullable=true)
     */
    private $kitchenStart;

    /**
     * @var \DateTime
     * @ORM\Column(name="kitchen_end", type="datetime", nullable=true)
     */
    private $kitchenEnd;

    /**
     * @var \DateTime
     * @ORM\Column(name="order_end", type="datetime", nullable=true)
     */
    private $orderEnd;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return \DateTime
     */
    public function getOrderStart(): \DateTime
    {
        return $this->orderStart;
    }

    /**
     * @param \DateTime $order_start
     */
    public function setOrderStart(\DateTime $order_start)
    {
        $this->orderStart = $order_start;
    }

    /**
     * @return \DateTime
     */
    public function getKitchenStart(): \DateTime
    {
        return $this->kitchenStart;
    }

    /**
     * @param \DateTime $kitchen_start
     */
    public function setKitchenStart(\DateTime $kitchen_start)
    {
        $this->kitchenStart = $kitchen_start;
    }

    /**
     * @return \DateTime
     */
    public function getKitchenEnd(): \DateTime
    {
        return $this->kitchenEnd;
    }

    /**
     * @param \DateTime $kitchen_end
     */
    public function setKitchenEnd(\DateTime $kitchen_end)
    {
        $this->kitchenEnd = $kitchen_end;
    }

    /**
     * @return \DateTime
     */
    public function getOrderEnd(): \DateTime
    {
        return $this->orderEnd;
    }

    /**
     * @param \DateTime $order_end
     */
    public function setOrderEnd(\DateTime $order_end)
    {
        $this->orderEnd = $order_end;
    }
}
