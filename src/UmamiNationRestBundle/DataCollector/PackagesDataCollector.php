<?php

namespace UmamiNation\RestPlatform\UmamiNationRestBundle\DataCollector;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * Class PackagesDataCollector.
 */
class PackagesDataCollector extends DataCollector
{
    const PACKAGES = ['doctrine/orm', 'twig/twig', 'api-platform/core'];

    /**
     * Collects data for the given Request and Response.
     *
     * @param Request    $request   A Request instance
     * @param Response   $response  A Response instance
     * @param \Exception $exception An Exception instance
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
    }

    /**
     * Returns the name of the collector.
     *
     * @return string The collector name
     */
    public function getName(): string
    {
        return 'umami_nation.composer.packages';
    }

    /**
     * @return array
     */
    public function getPackages(): array
    {
        $composerLockFile = \json_decode(\file_get_contents('../composer.lock'), true);
        $composerPackages = \array_reduce(
            $composerLockFile['packages'],
            function ($current, $package) {
                if (\in_array($package['name'], static::PACKAGES, true)) {
                    $current[$package['name']] = [
                        'version' => $package['version'],
                        'homepage' => $package['homepage'],
                    ];
                }

                return $current;
            },
            []
        );

        return [
            'data' => $composerPackages,
        ];
    }
}
